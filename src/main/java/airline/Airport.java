/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author iuabd
 */
public class Airport {
    
    //attributes
    private String code;
    private String city;

    //getters and setters
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        Flight flight = new Flight();
        Airplane airplane = new Airplane();
        Airline airline = new Airline();
        
        return "Flight date: " + flight.getDate() +
            "\nFlight number: "+ flight.getNumber()+
            "\nAirplane id: " + airplane.getId() +
            "\nAirplane make: " + airplane.getMake() +
            "\nAirplane model: " + airplane.getModel() +
            "\nAirplane " + airplane.getNumOfSeat() +
            "\nAirline name: " + airline.getName() +
            "\nAirline code: " + airline.getShortCode() +
            "\nAirport location: " + getCity() + "\nAirport code: " + getCode();

    }
}
