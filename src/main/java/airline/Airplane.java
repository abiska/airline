/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author iuabd
 */
public class Airplane {
    
    //attributes
    private String make;
    private String model;
    private String id;
    private String numOfSeat;

    //getters and setters
    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getNumOfSeat() {
        return numOfSeat;
    }
    public void setNumOfSeat(String numOfSeat) {
        this.numOfSeat = numOfSeat;
    }
}
